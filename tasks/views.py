from django.shortcuts import render, redirect
from django.http import HttpResponse

from .models import Task
from .forms import TaskForm


def index(request):
    tasks = Task.objects.all()
    form = TaskForm()

    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/')


    context = {
        'tasks': tasks,
        'form': form,
    }
    return render(request, 'tasks/lists.html', context=context)

def update_task(request, pk):
    task = Task.objects.get(id=pk)

    return render(request, 'tasks/update_task.html')

