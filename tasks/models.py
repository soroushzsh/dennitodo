from django.db import models

class Task(models.Model):
    PRIORITY_CHOICES = [
        ('0', ''),
        ('1', 'Urgent And Important'),
        ('2', 'Not Urgent But Important'),
        ('3', 'Not important But Urgent'),
        ('4', 'Not Important And Not Urgent'),
    ]

    STATUS_CHOICES = [
        ('Todo', 'Todo'),
        ('Doing', 'Doing'),
        ('Done', 'Done'),
    ]


    # other fields can have here: assign to project, reminder, subtasks, due date
    title = models.CharField(max_length=255)
    pomodoro_number = models.IntegerField()
    priority = models.CharField(max_length=2, choices=PRIORITY_CHOICES, default='0')
    status = models.CharField(max_length=5, choices=STATUS_CHOICES, default='Todo')
    note = models.TextField(blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    completed_date = models.DateTimeField(blank=True, null=True)


    def __str__(self):
        return self.title
